//
//  main.m
//  mopubExemple
//
//  Created by laurine baillet on 06/06/2018.
//  Copyright © 2018 laurine baillet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
