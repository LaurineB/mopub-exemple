//
//  PublicMopubCollectionViewCell_iPad.m
//  Public
//
//  Created by BELLEROSE Odile on 04/05/2016.
//  Copyright © 2016 TEIXEIRA Yohan. All rights reserved.
//

#import "SharedMopubCollectionViewCell.h"
#import "Utils.h"

// ---------------------------------------------------------------------------------

@interface SharedMopubCollectionViewCell ()

@property (nonatomic, strong) IBOutlet UILabel      *streamTitleLabel;
@property (nonatomic, strong) IBOutlet UIImageView  *streamImageView;
@property (nonatomic, strong) IBOutlet UILabel      *streamSponseredLabel;
@property (nonatomic, strong) IBOutlet UILabel      *streamSourceLabel;

@end

// ---------------------------------------------------------------------------------

@implementation SharedMopubCollectionViewCell

// ---------------------------------------------------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.streamTitleLabel.font      = [UIFont publicMediumFontWithSize:14];
    self.streamTitleLabel.textColor = [UIColor publicGrey20];
    
    self.streamSponseredLabel.font      = [UIFont publicBoldFontWithSize:8];
    self.streamSponseredLabel.textColor = [UIColor publicPurple];
    
    self.streamSourceLabel.font      = [UIFont publicRegularFontWithSize:11];
    self.streamSourceLabel.textColor = [UIColor publicPurple];
    
    //self.streamImageView.frame = CGRectMake(6.0, 10.0, 92.0, 68.0);
    self.streamImageView.clipsToBounds = TRUE;
    [self.streamImageView setContentMode:UIViewContentModeScaleAspectFill];
    
    self.contentView.backgroundColor          = [UIColor colorWithRed:236/255.f green:0/255.f blue:140/255.f alpha:0.1];
    
    self.streamSponseredLabel.text =  i18n(@"SPONSORISÉ");
    
//#ifdef DEBUG
//    self.layer.borderWidth = 1.f;
//    self.layer.borderColor = [UIColor greenColor].CGColor;
//#endif
}

// ---------------------------------------------------------------------------------

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

// ---------------------------------------------------------------------------------

- (UIImageView *)nativeMainImageView
{
    return self.streamImageView;
}

// ---------------------------------------------------------------------------------

- (UILabel *)nativeTitleTextLabel
{
    return self.streamTitleLabel;
}

// ---------------------------------------------------------------------------------

- (UILabel *)nativeMainTextLabel
{
    return self.streamSourceLabel;
}

// ---------------------------------------------------------------------------------

+ (UINib *)nibForAd
{
    return [UINib nibWithNibName:@"SharedMopubCollectionViewCell" bundle:nil];
}

// ---------------------------------------------------------------------------------

@end
