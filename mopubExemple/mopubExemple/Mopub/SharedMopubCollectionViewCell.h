//
//  PublicMopubCollectionViewCell_iPad.h
//  Public
//
//  Created by BELLEROSE Odile on 04/05/2016.
//  Copyright © 2016 TEIXEIRA Yohan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPNativeAdRendering.h"

@interface SharedMopubCollectionViewCell : UICollectionViewCell<MPNativeAdRendering>

@end
