//
//  AppDelegate.h
//  mopubExemple
//
//  Created by laurine baillet on 06/06/2018.
//  Copyright © 2018 laurine baillet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

