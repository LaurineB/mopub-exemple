//
//  ViewController.m
//  mopubExemple
//
//  Created by laurine baillet on 06/06/2018.
//  Copyright © 2018 laurine baillet. All rights reserved.
//

#import "ViewController.h"

#import "MPServerAdPositioning.h"
#import "MPAdView.h"
#import "MPNativeAdRequestTargeting.h"
#import "MPNativeAdConstants.h"
#import "MPStreamAdPlacer.h"
#import "MPCollectionViewAdPlacer.h"
#import "MPStaticNativeAdRendererSettings.h"
#import "MPNativeAdRendererConfiguration.h"
#import "MPStaticNativeAdRenderer.h"
#import "SharedMopubCollectionViewCell.h"

@interface ViewController () <MPAdViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) IBOutlet UICollectionView * articleCollectionView;


//Mopub
@property (nonatomic,strong ) MPCollectionViewAdPlacer              *placer;
@property (nonatomic,strong ) MPNativeAdRequestTargeting            *targeting;
@property (nonatomic, strong) NSString * mopubId;

//Datas
@property (nonatomic, strong) CarouselComponentCollectionViewCell   *carousel;
@property (strong, nonatomic) NSArray                               *carouselList;
@property (nonatomic, strong) NSMutableArray                        *realArticleList;

@property (nonatomic, assign) int                                   ligatusNewsFeedHeight;
@property (nonatomic, assign) BOOL                                  ligatusRequestIsInProgress;

@end

// ---------------------------------------------------------------------------------

@implementation ViewController

// ---------------------------------------------------------------------------------
#pragma mark init Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureViews];
    [self configureDatas];
}

// ---------------------------------------------------------------------------------

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([self firstRubricIsSelected])
    {
        [self updateLigatusCellIsLoad:FALSE];
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - Layout and configurations


// ---------------------------------------------------------------------------------

- (void)reloadCollectionView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self.delegate disableAds])
            [self.articleCollectionView reloadData];
        else
            [self.articleCollectionView mp_reloadData];
    });
}

// ---------------------------------------------------------------------------------

- (void)configureViews
{
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UINib *cellNib = [UINib nibWithNibName:@"SharedNewsCollectionViewCell" bundle:nil];
    [self.articleCollectionView registerNib:cellNib forCellWithReuseIdentifier:PublicNewsSharedCellIdentifier];
    
    // [...]
}

// ---------------------------------------------------------------------------------

- (void)configureDatas
{
    if(self.rubric)
        [self loadDataForCurrentRubric:[self.rubric.titre lowercaseString]];
    else
        [self loadDataForCurrentRubric:self.rubrikNameSelected];
    
        if( ![self.delegate disableAds])
        {
            [self.articleCollectionView mp_setDelegate:self];
            [self.articleCollectionView mp_setDataSource:self];
            
            MPStaticNativeAdRendererSettings *nativeAdSettings = [[MPStaticNativeAdRendererSettings alloc] init];
            nativeAdSettings.renderingViewClass = [SharedMopubCollectionViewCell class];
            
            nativeAdSettings.viewSizeHandler = ^(CGFloat maximumWidth) {
                return CGSizeMake(self.articleCollectionView.width, 104);
            };
            
            MPNativeAdRendererConfiguration *nativeAdConfig = [MPStaticNativeAdRenderer rendererConfigurationWithRendererSettings:nativeAdSettings];
            nativeAdConfig.supportedCustomEvents=[nativeAdConfig.supportedCustomEvents arrayByAddingObject:@"FlurryNativeCustomEvent"];
            
            self.placer = [MPCollectionViewAdPlacer placerWithCollectionView:self.articleCollectionView viewController:self rendererConfigurations:@[nativeAdConfig]];
            
            if ([Utils appIsActive])
                [self.placer loadAdsForAdUnitID:self.mopubId targeting:nil];
        }
        else
        {
            [self.articleCollectionView setDelegate:self];
            [self.articleCollectionView setDataSource:self];
        }
    
}

// ---------------------------------------------------------------------------------

- (void)loadDataForCurrentRubric:(NSString*)rubricName
{
    [super loadDataForCurrentRubric:rubricName];
    
    self.articleList = [[NSMutableArray alloc] initWithArray:[[[DataManager sharedManager]allArticleHash]objectForKey:[rubricName lowercaseString]]];
    
    if(!self.delegate.disableAds)
    {
        [self.articleCollectionView mp_reloadData];
        
        if ([Utils appIsActive])
        {
            if(self.placer)
                [self.placer loadAdsForAdUnitID:self.mopubId  targeting:nil];
        }
    }
    else
    {
        [self.articleCollectionView reloadData];
    }
    
    [self displayNoContentViewIfNeeded];
}


// ---------------------------------------------------------------------------------
#pragma mark - Collection View delegate & datasource

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}

// ---------------------------------------------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([self.articleList count] >= 20)
    {
        return [self.articleList count] + (self.shouldLoadMoreData ? 1 : 0);
    }
    else
    {
        return  [self.articleList count] ;
    }
}

// ---------------------------------------------------------------------------------

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.articleList count] ==  indexPath.row) // Load more Cell case
    {
        SharedLoadMoreCollectionViewCell *moreCell = (SharedLoadMoreCollectionViewCell*)[collectionView mp_dequeueReusableCellWithReuseIdentifier:SharedLoadMoreCellIdentifier forIndexPath:indexPath];
        self.offset += 20;
        
        __weak typeof(self) weakSelf = self;
        
        [[DataManager sharedManager] getMoreArticleForCategory:[self urlEntryForRubrik:[self.rubric.titre lowercaseString] ] withOffset:self.offset andLimit:self.limit withSuccess:^(CategorieModel *categorie) {
            
            if( [categorie.nbFounds integerValue] > [weakSelf.articleList count])
            {
                [weakSelf.articleList addObjectsFromArray:categorie.articleList];
                weakSelf.shouldLoadMoreData = YES;
            }
            else
            {
                weakSelf.shouldLoadMoreData = NO;
            }
            
            [weakSelf reloadCollectionView];
            
        } failure:^{
            weakSelf.shouldLoadMoreData = NO;
            [weakSelf reloadCollectionView];
        }];
        
        return  moreCell;
    }
    
    id object = [self.articleList objectAtIndex:indexPath.row];
    
    if([object isKindOfClass:[ArticleModel class]])
    {
        ArticleModel * article = (ArticleModel*)object;
        
        if([article isNativeAd]) // Article content sponsored by ... cell case
        {
            SharedNewsSponsoCollectionViewCell * cell = (SharedNewsSponsoCollectionViewCell*)[collectionView mp_dequeueReusableCellWithReuseIdentifier:PublicNewsSponsoSharedCellIdentifier forIndexPath:indexPath];
            [cell configureWithArticle:article];
            
            return cell;
        }
        //[...]
    }
}
